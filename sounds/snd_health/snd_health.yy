{
  "conversionMode": 0,
  "compression": 0,
  "volume": 0.47,
  "preload": true,
  "bitRate": 192,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "snd_health",
  "duration": 0.0,
  "parent": {
    "name": "sfx",
    "path": "folders/Sounds/sfx.yy",
  },
  "resourceVersion": "1.0",
  "name": "snd_health",
  "tags": [],
  "resourceType": "GMSound",
}