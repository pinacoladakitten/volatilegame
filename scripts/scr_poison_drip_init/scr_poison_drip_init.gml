function scr_poison_drip_init() {
	//Initialize Particle
	global.pt_pdrip = part_type_create();
	//Settings
	part_type_sprite(global.pt_pdrip, spr_player_wep_eff, true, false, false);
	part_type_size(global.pt_pdrip, 1, 1, 0, 0);
	part_type_speed(global.pt_pdrip, 0.4, 0.4, 0, 0);
	part_type_direction(global.pt_pdrip,0,360,0,0)
	part_type_life(global.pt_pdrip, 15, 15);



}
