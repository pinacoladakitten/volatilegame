function scr_smshatter_init() {
	//Initialize Particle
	global.pt_smshatter = part_type_create();
	//Settings
	part_type_sprite(global.pt_smshatter, eff_mirror_shatters, true, false, true);
	part_type_size(global.pt_smshatter, 0.5, 0.6, 0.05, 0);
	part_type_speed(global.pt_smshatter, 0.4, 0.4, 0, 0);
	part_type_direction(global.pt_smshatter,0,360,0,0)
	part_type_life(global.pt_smshatter, 25, 25);
	part_type_orientation(global.pt_smshatter,0,360,0,0,0)



}
