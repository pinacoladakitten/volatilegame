function __global_object_depths() {
	// Initialise the global array that allows the lookup of the depth of a given object
	// GM2.0 does not have a depth on objects so on import from 1.x a global array is created
	// NOTE: MacroExpansion is used to insert the array initialisation at import time
	gml_pragma( "global", "__global_object_depths()");

	// insert the generated arrays here
	global.__objectDepths[0] = -19; // obj_player
	global.__objectDepths[1] = 0; // player_stats
	global.__objectDepths[2] = -100; // particle_sys
	global.__objectDepths[3] = 0; // global_variables
	global.__objectDepths[4] = 0; // obj_fireball_mag
	global.__objectDepths[5] = -99; // obj_starball_mag
	global.__objectDepths[6] = 0; // obj_seed_mag
	global.__objectDepths[7] = 0; // obj_starblade_mag
	global.__objectDepths[8] = 0; // obj_lightdash_mag
	global.__objectDepths[9] = 0; // obj_poisonball
	global.__objectDepths[10] = -14; // obj_player_wep
	global.__objectDepths[11] = -11; // obj_player_mirror
	global.__objectDepths[12] = 0; // obj_mirror_shatter
	global.__objectDepths[13] = 0; // obj_mirror_mag
	global.__objectDepths[14] = -100; // obj_bat
	global.__objectDepths[15] = 0; // obj_zombie
	global.__objectDepths[16] = 0; // obj_snake
	global.__objectDepths[17] = 9; // obj_torch
	global.__objectDepths[18] = -12; // obj_lantern
	global.__objectDepths[19] = -1000; // obj_lighting
	global.__objectDepths[20] = -1000; // obj_transition1
	global.__objectDepths[21] = -1000; // obj_transition2
	global.__objectDepths[22] = 0; // obj_background_scroll01
	global.__objectDepths[23] = 0; // obj_screen_shake01
	global.__objectDepths[24] = 0; // obj_eff_doublej
	global.__objectDepths[25] = 0; // obj_eff_blood
	global.__objectDepths[26] = -100; // obj_eff_swhit01
	global.__objectDepths[27] = 0; // obj_afterimage
	global.__objectDepths[28] = -20; // obj_poison
	global.__objectDepths[29] = -100; // obj_guillotine
	global.__objectDepths[30] = 0; // obj_item_mana
	global.__objectDepths[31] = 0; // obj_item_health
	global.__objectDepths[32] = 0; // obj_cursor
	global.__objectDepths[33] = 0; // obj_press_enter
	global.__objectDepths[34] = -10000; // obj_form_select
	global.__objectDepths[35] = -1000; // obj_level_title
	global.__objectDepths[36] = -10000; // obj_dia_event01
	global.__objectDepths[37] = 0; // obj_trigger_dia01
	global.__objectDepths[38] = -20; // obj_grass
	global.__objectDepths[39] = 0; // obj_col
	global.__objectDepths[40] = 9; // obj_door
	global.__objectDepths[41] = 0; // death_collision


	global.__objectNames[0] = "obj_player";
	global.__objectNames[1] = "player_stats";
	global.__objectNames[2] = "particle_sys";
	global.__objectNames[3] = "global_variables";
	global.__objectNames[4] = "obj_fireball_mag";
	global.__objectNames[5] = "obj_starball_mag";
	global.__objectNames[6] = "obj_seed_mag";
	global.__objectNames[7] = "obj_starblade_mag";
	global.__objectNames[8] = "obj_lightdash_mag";
	global.__objectNames[9] = "obj_poisonball";
	global.__objectNames[10] = "obj_player_wep";
	global.__objectNames[11] = "obj_player_mirror";
	global.__objectNames[12] = "obj_mirror_shatter";
	global.__objectNames[13] = "obj_mirror_mag";
	global.__objectNames[14] = "obj_bat";
	global.__objectNames[15] = "obj_zombie";
	global.__objectNames[16] = "obj_snake";
	global.__objectNames[17] = "obj_torch";
	global.__objectNames[18] = "obj_lantern";
	global.__objectNames[19] = "obj_lighting";
	global.__objectNames[20] = "obj_transition1";
	global.__objectNames[21] = "obj_transition2";
	global.__objectNames[22] = "obj_background_scroll01";
	global.__objectNames[23] = "obj_screen_shake01";
	global.__objectNames[24] = "obj_eff_doublej";
	global.__objectNames[25] = "obj_eff_blood";
	global.__objectNames[26] = "obj_eff_swhit01";
	global.__objectNames[27] = "obj_afterimage";
	global.__objectNames[28] = "obj_poison";
	global.__objectNames[29] = "obj_guillotine";
	global.__objectNames[30] = "obj_item_mana";
	global.__objectNames[31] = "obj_item_health";
	global.__objectNames[32] = "obj_cursor";
	global.__objectNames[33] = "obj_press_enter";
	global.__objectNames[34] = "obj_form_select";
	global.__objectNames[35] = "obj_level_title";
	global.__objectNames[36] = "obj_dia_event01";
	global.__objectNames[37] = "obj_trigger_dia01";
	global.__objectNames[38] = "obj_grass";
	global.__objectNames[39] = "obj_col";
	global.__objectNames[40] = "obj_door";
	global.__objectNames[41] = "death_collision";


	// create another array that has the correct entries
	var len = array_length_1d(global.__objectDepths);
	global.__objectID2Depth = [];
	for( var i=0; i<len; ++i ) {
		var objID = asset_get_index( global.__objectNames[i] );
		if (objID >= 0) {
			global.__objectID2Depth[ objID ] = global.__objectDepths[i];
		} // end if
	} // end for


}
