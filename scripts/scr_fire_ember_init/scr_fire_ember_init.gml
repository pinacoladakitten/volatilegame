function scr_fire_ember_init() {
	//Initialize Particle
	global.pt_ember = part_type_create();
	//Settings
	part_type_sprite(global.pt_ember, eff_fire_ember, true, false, true);
	part_type_size(global.pt_ember, 1, 1.2, -0.02, 0);
	part_type_speed(global.pt_ember, 0.4, 0.4, 0, 0);
	part_type_direction(global.pt_ember,0,360,0,0)
	part_type_life(global.pt_ember, 25, 50);



}
