function scr_fire_init() {
	//Initialize Particle
	global.pt_fire = part_type_create();
	//Settings
	part_type_sprite(global.pt_fire, spr_part_fire, true, false, true);
	part_type_size(global.pt_fire, 1, 1.2, -0.02, 0);
	part_type_speed(global.pt_fire, 0.4, 0.4, 0, 0);
	part_type_direction(global.pt_fire,0,360,0,0)
	part_type_life(global.pt_fire, 25, 50);



}
