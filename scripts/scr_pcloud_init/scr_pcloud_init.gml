function scr_pcloud_init() {
	//Initialize Particle
	global.pt_pcloud = part_type_create();
	//Settings
	part_type_sprite(global.pt_pcloud, eff_poison_cloud, true, false, true);
	part_type_size(global.pt_pcloud, 0.5, 0.6, 0.007, 0);
	part_type_speed(global.pt_pcloud, 0.4, 0.4, 0, 0);
	part_type_direction(global.pt_pcloud,0,360,0,0)
	part_type_life(global.pt_pcloud, 25, 50);



}
