{
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "bboxMode": 2,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 12,
  "bbox_right": 38,
  "bbox_top": 0,
  "bbox_bottom": 49,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 50,
  "height": 60,
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"4a5507a3-30db-4106-ac4f-623b78231aba","path":"sprites/spr_player_dash_m/spr_player_dash_m.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"4a5507a3-30db-4106-ac4f-623b78231aba","path":"sprites/spr_player_dash_m/spr_player_dash_m.yy",},"LayerId":{"name":"d1cc920d-bcf5-4e16-911a-daad789184b6","path":"sprites/spr_player_dash_m/spr_player_dash_m.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_dash_m","path":"sprites/spr_player_dash_m/spr_player_dash_m.yy",},"resourceVersion":"1.0","name":"4a5507a3-30db-4106-ac4f-623b78231aba","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"7f12968c-012f-4c28-8ce4-54789150cf0f","path":"sprites/spr_player_dash_m/spr_player_dash_m.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"7f12968c-012f-4c28-8ce4-54789150cf0f","path":"sprites/spr_player_dash_m/spr_player_dash_m.yy",},"LayerId":{"name":"d1cc920d-bcf5-4e16-911a-daad789184b6","path":"sprites/spr_player_dash_m/spr_player_dash_m.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_dash_m","path":"sprites/spr_player_dash_m/spr_player_dash_m.yy",},"resourceVersion":"1.0","name":"7f12968c-012f-4c28-8ce4-54789150cf0f","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e608c97f-061f-423a-bcdb-653133d60b64","path":"sprites/spr_player_dash_m/spr_player_dash_m.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e608c97f-061f-423a-bcdb-653133d60b64","path":"sprites/spr_player_dash_m/spr_player_dash_m.yy",},"LayerId":{"name":"d1cc920d-bcf5-4e16-911a-daad789184b6","path":"sprites/spr_player_dash_m/spr_player_dash_m.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_dash_m","path":"sprites/spr_player_dash_m/spr_player_dash_m.yy",},"resourceVersion":"1.0","name":"e608c97f-061f-423a-bcdb-653133d60b64","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"cca5d32a-f5be-48a6-a55a-3379797e7a00","path":"sprites/spr_player_dash_m/spr_player_dash_m.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"cca5d32a-f5be-48a6-a55a-3379797e7a00","path":"sprites/spr_player_dash_m/spr_player_dash_m.yy",},"LayerId":{"name":"d1cc920d-bcf5-4e16-911a-daad789184b6","path":"sprites/spr_player_dash_m/spr_player_dash_m.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_dash_m","path":"sprites/spr_player_dash_m/spr_player_dash_m.yy",},"resourceVersion":"1.0","name":"cca5d32a-f5be-48a6-a55a-3379797e7a00","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"9c5be1fe-ccf7-492f-9e2c-91757d3ff778","path":"sprites/spr_player_dash_m/spr_player_dash_m.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"9c5be1fe-ccf7-492f-9e2c-91757d3ff778","path":"sprites/spr_player_dash_m/spr_player_dash_m.yy",},"LayerId":{"name":"d1cc920d-bcf5-4e16-911a-daad789184b6","path":"sprites/spr_player_dash_m/spr_player_dash_m.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_dash_m","path":"sprites/spr_player_dash_m/spr_player_dash_m.yy",},"resourceVersion":"1.0","name":"9c5be1fe-ccf7-492f-9e2c-91757d3ff778","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"5db43309-c225-4d4f-8c8d-c3dfd072142b","path":"sprites/spr_player_dash_m/spr_player_dash_m.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"5db43309-c225-4d4f-8c8d-c3dfd072142b","path":"sprites/spr_player_dash_m/spr_player_dash_m.yy",},"LayerId":{"name":"d1cc920d-bcf5-4e16-911a-daad789184b6","path":"sprites/spr_player_dash_m/spr_player_dash_m.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_dash_m","path":"sprites/spr_player_dash_m/spr_player_dash_m.yy",},"resourceVersion":"1.0","name":"5db43309-c225-4d4f-8c8d-c3dfd072142b","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_player_dash_m","path":"sprites/spr_player_dash_m/spr_player_dash_m.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 1.0,
    "playbackSpeedType": 1,
    "autoRecord": true,
    "volume": 1.0,
    "length": 6.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"047026f4-cd8f-4c36-b005-a6e82a9db3f4","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"4a5507a3-30db-4106-ac4f-623b78231aba","path":"sprites/spr_player_dash_m/spr_player_dash_m.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"ea3f2585-c18b-4bae-b098-e42de0137f82","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"7f12968c-012f-4c28-8ce4-54789150cf0f","path":"sprites/spr_player_dash_m/spr_player_dash_m.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"82cad1ae-9d40-4cf0-b8a5-6e9fb33ee2f1","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e608c97f-061f-423a-bcdb-653133d60b64","path":"sprites/spr_player_dash_m/spr_player_dash_m.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"dc656878-590e-47b4-8a1f-a488eeb939b9","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"cca5d32a-f5be-48a6-a55a-3379797e7a00","path":"sprites/spr_player_dash_m/spr_player_dash_m.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"8c65e32a-417d-4182-9274-65e333168b8f","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"9c5be1fe-ccf7-492f-9e2c-91757d3ff778","path":"sprites/spr_player_dash_m/spr_player_dash_m.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"53b9893f-c040-46a3-b019-1e54dde820d7","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"5db43309-c225-4d4f-8c8d-c3dfd072142b","path":"sprites/spr_player_dash_m/spr_player_dash_m.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 25,
    "yorigin": 25,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_player_dash_m","path":"sprites/spr_player_dash_m/spr_player_dash_m.yy",},
    "resourceVersion": "1.4",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"d1cc920d-bcf5-4e16-911a-daad789184b6","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "mirror",
    "path": "folders/Sprites/player_sprites/mirror.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_player_dash_m",
  "tags": [],
  "resourceType": "GMSprite",
}