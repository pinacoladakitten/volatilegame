{
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "bboxMode": 2,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 12,
  "bbox_right": 38,
  "bbox_top": 0,
  "bbox_bottom": 49,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 50,
  "height": 50,
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"dfa92957-fc51-4f7e-aa40-f315b6d71ce1","path":"sprites/spr_player_walk_swing/spr_player_walk_swing.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"dfa92957-fc51-4f7e-aa40-f315b6d71ce1","path":"sprites/spr_player_walk_swing/spr_player_walk_swing.yy",},"LayerId":{"name":"2e1fe3b0-4524-4e2f-877f-f247d8b015c8","path":"sprites/spr_player_walk_swing/spr_player_walk_swing.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_walk_swing","path":"sprites/spr_player_walk_swing/spr_player_walk_swing.yy",},"resourceVersion":"1.0","name":"dfa92957-fc51-4f7e-aa40-f315b6d71ce1","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"8b5caae6-9cd0-45dc-bbe4-f4292fdc2f9c","path":"sprites/spr_player_walk_swing/spr_player_walk_swing.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"8b5caae6-9cd0-45dc-bbe4-f4292fdc2f9c","path":"sprites/spr_player_walk_swing/spr_player_walk_swing.yy",},"LayerId":{"name":"2e1fe3b0-4524-4e2f-877f-f247d8b015c8","path":"sprites/spr_player_walk_swing/spr_player_walk_swing.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_walk_swing","path":"sprites/spr_player_walk_swing/spr_player_walk_swing.yy",},"resourceVersion":"1.0","name":"8b5caae6-9cd0-45dc-bbe4-f4292fdc2f9c","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"8ed5e854-aace-4e05-98a7-95614a9cecfc","path":"sprites/spr_player_walk_swing/spr_player_walk_swing.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"8ed5e854-aace-4e05-98a7-95614a9cecfc","path":"sprites/spr_player_walk_swing/spr_player_walk_swing.yy",},"LayerId":{"name":"2e1fe3b0-4524-4e2f-877f-f247d8b015c8","path":"sprites/spr_player_walk_swing/spr_player_walk_swing.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_walk_swing","path":"sprites/spr_player_walk_swing/spr_player_walk_swing.yy",},"resourceVersion":"1.0","name":"8ed5e854-aace-4e05-98a7-95614a9cecfc","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e0cbcb39-533b-4ed6-ace3-d5527e28dcb6","path":"sprites/spr_player_walk_swing/spr_player_walk_swing.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e0cbcb39-533b-4ed6-ace3-d5527e28dcb6","path":"sprites/spr_player_walk_swing/spr_player_walk_swing.yy",},"LayerId":{"name":"2e1fe3b0-4524-4e2f-877f-f247d8b015c8","path":"sprites/spr_player_walk_swing/spr_player_walk_swing.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_walk_swing","path":"sprites/spr_player_walk_swing/spr_player_walk_swing.yy",},"resourceVersion":"1.0","name":"e0cbcb39-533b-4ed6-ace3-d5527e28dcb6","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"cb6f9e4b-e9b1-4343-b0aa-6612ebe38aaa","path":"sprites/spr_player_walk_swing/spr_player_walk_swing.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"cb6f9e4b-e9b1-4343-b0aa-6612ebe38aaa","path":"sprites/spr_player_walk_swing/spr_player_walk_swing.yy",},"LayerId":{"name":"2e1fe3b0-4524-4e2f-877f-f247d8b015c8","path":"sprites/spr_player_walk_swing/spr_player_walk_swing.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_walk_swing","path":"sprites/spr_player_walk_swing/spr_player_walk_swing.yy",},"resourceVersion":"1.0","name":"cb6f9e4b-e9b1-4343-b0aa-6612ebe38aaa","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_player_walk_swing","path":"sprites/spr_player_walk_swing/spr_player_walk_swing.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 1.0,
    "playbackSpeedType": 1,
    "autoRecord": true,
    "volume": 1.0,
    "length": 5.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"be8fc472-8f69-4217-b140-296889ed3582","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"dfa92957-fc51-4f7e-aa40-f315b6d71ce1","path":"sprites/spr_player_walk_swing/spr_player_walk_swing.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"2ec4e9ce-36f8-468d-a5ec-58a31dca475c","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"8b5caae6-9cd0-45dc-bbe4-f4292fdc2f9c","path":"sprites/spr_player_walk_swing/spr_player_walk_swing.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"b8c27ee2-af8d-46fc-8ff1-1bc3b526a4d4","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"8ed5e854-aace-4e05-98a7-95614a9cecfc","path":"sprites/spr_player_walk_swing/spr_player_walk_swing.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"8544c45a-d454-4ff1-8d9e-fd534d15ade6","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e0cbcb39-533b-4ed6-ace3-d5527e28dcb6","path":"sprites/spr_player_walk_swing/spr_player_walk_swing.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"4077df28-9b41-4d98-b457-1e3f206a2ed3","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"cb6f9e4b-e9b1-4343-b0aa-6612ebe38aaa","path":"sprites/spr_player_walk_swing/spr_player_walk_swing.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 25,
    "yorigin": 25,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_player_walk_swing","path":"sprites/spr_player_walk_swing/spr_player_walk_swing.yy",},
    "resourceVersion": "1.4",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"2e1fe3b0-4524-4e2f-877f-f247d8b015c8","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "player_sprites",
    "path": "folders/Sprites/player_sprites.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_player_walk_swing",
  "tags": [],
  "resourceType": "GMSprite",
}