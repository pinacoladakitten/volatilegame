{
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 0,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 41,
  "bbox_top": 0,
  "bbox_bottom": 41,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 42,
  "height": 42,
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"eba47e93-06d1-4cc7-bef9-43bfd6fb3db8","path":"sprites/spr_fire_icon_tab/spr_fire_icon_tab.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"eba47e93-06d1-4cc7-bef9-43bfd6fb3db8","path":"sprites/spr_fire_icon_tab/spr_fire_icon_tab.yy",},"LayerId":{"name":"fe5d7499-ca8d-418b-9299-bac796607cdf","path":"sprites/spr_fire_icon_tab/spr_fire_icon_tab.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_fire_icon_tab","path":"sprites/spr_fire_icon_tab/spr_fire_icon_tab.yy",},"resourceVersion":"1.0","name":"eba47e93-06d1-4cc7-bef9-43bfd6fb3db8","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"ca7bd2a5-7b99-45e9-a753-536ad1d4b9b1","path":"sprites/spr_fire_icon_tab/spr_fire_icon_tab.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"ca7bd2a5-7b99-45e9-a753-536ad1d4b9b1","path":"sprites/spr_fire_icon_tab/spr_fire_icon_tab.yy",},"LayerId":{"name":"fe5d7499-ca8d-418b-9299-bac796607cdf","path":"sprites/spr_fire_icon_tab/spr_fire_icon_tab.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_fire_icon_tab","path":"sprites/spr_fire_icon_tab/spr_fire_icon_tab.yy",},"resourceVersion":"1.0","name":"ca7bd2a5-7b99-45e9-a753-536ad1d4b9b1","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e8fbd36d-bf7b-4c18-8848-5b32e720f2b4","path":"sprites/spr_fire_icon_tab/spr_fire_icon_tab.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e8fbd36d-bf7b-4c18-8848-5b32e720f2b4","path":"sprites/spr_fire_icon_tab/spr_fire_icon_tab.yy",},"LayerId":{"name":"fe5d7499-ca8d-418b-9299-bac796607cdf","path":"sprites/spr_fire_icon_tab/spr_fire_icon_tab.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_fire_icon_tab","path":"sprites/spr_fire_icon_tab/spr_fire_icon_tab.yy",},"resourceVersion":"1.0","name":"e8fbd36d-bf7b-4c18-8848-5b32e720f2b4","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_fire_icon_tab","path":"sprites/spr_fire_icon_tab/spr_fire_icon_tab.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 1.0,
    "playbackSpeedType": 1,
    "autoRecord": true,
    "volume": 1.0,
    "length": 3.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"e27671ef-fb4b-4d3b-b50b-514d86cfc113","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"eba47e93-06d1-4cc7-bef9-43bfd6fb3db8","path":"sprites/spr_fire_icon_tab/spr_fire_icon_tab.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"50e868dd-c74e-4269-ac5e-8ce979a16f65","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ca7bd2a5-7b99-45e9-a753-536ad1d4b9b1","path":"sprites/spr_fire_icon_tab/spr_fire_icon_tab.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"ef72e7e5-683a-4e1e-9652-c92c3beea159","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e8fbd36d-bf7b-4c18-8848-5b32e720f2b4","path":"sprites/spr_fire_icon_tab/spr_fire_icon_tab.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_fire_icon_tab","path":"sprites/spr_fire_icon_tab/spr_fire_icon_tab.yy",},
    "resourceVersion": "1.4",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"fe5d7499-ca8d-418b-9299-bac796607cdf","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "form_interface",
    "path": "folders/Sprites/HUD/form_interface.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_fire_icon_tab",
  "tags": [],
  "resourceType": "GMSprite",
}