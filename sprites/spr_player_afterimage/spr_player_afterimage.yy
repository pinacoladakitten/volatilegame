{
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "bboxMode": 2,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 12,
  "bbox_right": 38,
  "bbox_top": 0,
  "bbox_bottom": 49,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 50,
  "height": 60,
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"96f9c9e9-c2ca-49b8-bb14-f2523d4305e0","path":"sprites/spr_player_afterimage/spr_player_afterimage.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"96f9c9e9-c2ca-49b8-bb14-f2523d4305e0","path":"sprites/spr_player_afterimage/spr_player_afterimage.yy",},"LayerId":{"name":"a8ce73b4-26f4-4b7a-91b7-8c68e2633bd8","path":"sprites/spr_player_afterimage/spr_player_afterimage.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_afterimage","path":"sprites/spr_player_afterimage/spr_player_afterimage.yy",},"resourceVersion":"1.0","name":"96f9c9e9-c2ca-49b8-bb14-f2523d4305e0","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"c1fc1cbc-2cf4-4d75-8ef2-56a6c042bb9a","path":"sprites/spr_player_afterimage/spr_player_afterimage.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c1fc1cbc-2cf4-4d75-8ef2-56a6c042bb9a","path":"sprites/spr_player_afterimage/spr_player_afterimage.yy",},"LayerId":{"name":"a8ce73b4-26f4-4b7a-91b7-8c68e2633bd8","path":"sprites/spr_player_afterimage/spr_player_afterimage.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_afterimage","path":"sprites/spr_player_afterimage/spr_player_afterimage.yy",},"resourceVersion":"1.0","name":"c1fc1cbc-2cf4-4d75-8ef2-56a6c042bb9a","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"6f48cc5f-9b98-4814-a374-0e7f408c26ef","path":"sprites/spr_player_afterimage/spr_player_afterimage.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"6f48cc5f-9b98-4814-a374-0e7f408c26ef","path":"sprites/spr_player_afterimage/spr_player_afterimage.yy",},"LayerId":{"name":"a8ce73b4-26f4-4b7a-91b7-8c68e2633bd8","path":"sprites/spr_player_afterimage/spr_player_afterimage.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_afterimage","path":"sprites/spr_player_afterimage/spr_player_afterimage.yy",},"resourceVersion":"1.0","name":"6f48cc5f-9b98-4814-a374-0e7f408c26ef","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"8fe98ed2-c520-4615-a6b7-e139e76df399","path":"sprites/spr_player_afterimage/spr_player_afterimage.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"8fe98ed2-c520-4615-a6b7-e139e76df399","path":"sprites/spr_player_afterimage/spr_player_afterimage.yy",},"LayerId":{"name":"a8ce73b4-26f4-4b7a-91b7-8c68e2633bd8","path":"sprites/spr_player_afterimage/spr_player_afterimage.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_afterimage","path":"sprites/spr_player_afterimage/spr_player_afterimage.yy",},"resourceVersion":"1.0","name":"8fe98ed2-c520-4615-a6b7-e139e76df399","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"f5d60461-ac18-4f33-98e2-6635b335d3d2","path":"sprites/spr_player_afterimage/spr_player_afterimage.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"f5d60461-ac18-4f33-98e2-6635b335d3d2","path":"sprites/spr_player_afterimage/spr_player_afterimage.yy",},"LayerId":{"name":"a8ce73b4-26f4-4b7a-91b7-8c68e2633bd8","path":"sprites/spr_player_afterimage/spr_player_afterimage.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_afterimage","path":"sprites/spr_player_afterimage/spr_player_afterimage.yy",},"resourceVersion":"1.0","name":"f5d60461-ac18-4f33-98e2-6635b335d3d2","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"3624a66a-07be-413b-9e18-3f35f0868431","path":"sprites/spr_player_afterimage/spr_player_afterimage.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"3624a66a-07be-413b-9e18-3f35f0868431","path":"sprites/spr_player_afterimage/spr_player_afterimage.yy",},"LayerId":{"name":"a8ce73b4-26f4-4b7a-91b7-8c68e2633bd8","path":"sprites/spr_player_afterimage/spr_player_afterimage.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_afterimage","path":"sprites/spr_player_afterimage/spr_player_afterimage.yy",},"resourceVersion":"1.0","name":"3624a66a-07be-413b-9e18-3f35f0868431","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"18829fe0-d13f-43a7-a8c3-44a96ab806a7","path":"sprites/spr_player_afterimage/spr_player_afterimage.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"18829fe0-d13f-43a7-a8c3-44a96ab806a7","path":"sprites/spr_player_afterimage/spr_player_afterimage.yy",},"LayerId":{"name":"a8ce73b4-26f4-4b7a-91b7-8c68e2633bd8","path":"sprites/spr_player_afterimage/spr_player_afterimage.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_afterimage","path":"sprites/spr_player_afterimage/spr_player_afterimage.yy",},"resourceVersion":"1.0","name":"18829fe0-d13f-43a7-a8c3-44a96ab806a7","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"07ea16f7-d3b1-417a-baf6-a9010fa9c350","path":"sprites/spr_player_afterimage/spr_player_afterimage.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"07ea16f7-d3b1-417a-baf6-a9010fa9c350","path":"sprites/spr_player_afterimage/spr_player_afterimage.yy",},"LayerId":{"name":"a8ce73b4-26f4-4b7a-91b7-8c68e2633bd8","path":"sprites/spr_player_afterimage/spr_player_afterimage.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_afterimage","path":"sprites/spr_player_afterimage/spr_player_afterimage.yy",},"resourceVersion":"1.0","name":"07ea16f7-d3b1-417a-baf6-a9010fa9c350","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e9ceb376-20bd-444c-880c-38acb2a7a4af","path":"sprites/spr_player_afterimage/spr_player_afterimage.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e9ceb376-20bd-444c-880c-38acb2a7a4af","path":"sprites/spr_player_afterimage/spr_player_afterimage.yy",},"LayerId":{"name":"a8ce73b4-26f4-4b7a-91b7-8c68e2633bd8","path":"sprites/spr_player_afterimage/spr_player_afterimage.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_afterimage","path":"sprites/spr_player_afterimage/spr_player_afterimage.yy",},"resourceVersion":"1.0","name":"e9ceb376-20bd-444c-880c-38acb2a7a4af","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"41925b29-015d-42a2-9e8e-e3f7061a65c8","path":"sprites/spr_player_afterimage/spr_player_afterimage.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"41925b29-015d-42a2-9e8e-e3f7061a65c8","path":"sprites/spr_player_afterimage/spr_player_afterimage.yy",},"LayerId":{"name":"a8ce73b4-26f4-4b7a-91b7-8c68e2633bd8","path":"sprites/spr_player_afterimage/spr_player_afterimage.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_afterimage","path":"sprites/spr_player_afterimage/spr_player_afterimage.yy",},"resourceVersion":"1.0","name":"41925b29-015d-42a2-9e8e-e3f7061a65c8","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_player_afterimage","path":"sprites/spr_player_afterimage/spr_player_afterimage.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 1.0,
    "playbackSpeedType": 1,
    "autoRecord": true,
    "volume": 1.0,
    "length": 10.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"41dfe8d1-e662-4e8e-95b0-94ba10c22fe2","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"96f9c9e9-c2ca-49b8-bb14-f2523d4305e0","path":"sprites/spr_player_afterimage/spr_player_afterimage.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"355c2dfe-d48f-4883-b1a8-feff163abac4","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c1fc1cbc-2cf4-4d75-8ef2-56a6c042bb9a","path":"sprites/spr_player_afterimage/spr_player_afterimage.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"373370fe-a9ad-426e-93a9-a9a5af789c0e","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"6f48cc5f-9b98-4814-a374-0e7f408c26ef","path":"sprites/spr_player_afterimage/spr_player_afterimage.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"50ecc958-d786-49fb-96c1-335549a24dca","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"8fe98ed2-c520-4615-a6b7-e139e76df399","path":"sprites/spr_player_afterimage/spr_player_afterimage.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"abd42539-18a0-4fb2-b546-a42f96d4649c","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f5d60461-ac18-4f33-98e2-6635b335d3d2","path":"sprites/spr_player_afterimage/spr_player_afterimage.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"1077b369-7a6a-441a-9b23-2233dc9e80ed","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"3624a66a-07be-413b-9e18-3f35f0868431","path":"sprites/spr_player_afterimage/spr_player_afterimage.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"632740b8-60ac-4a68-850d-77f2451b4e9a","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"18829fe0-d13f-43a7-a8c3-44a96ab806a7","path":"sprites/spr_player_afterimage/spr_player_afterimage.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"2622ccef-558c-44eb-b22d-c5650fa8ba84","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"07ea16f7-d3b1-417a-baf6-a9010fa9c350","path":"sprites/spr_player_afterimage/spr_player_afterimage.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"ddd34bb3-e754-484d-9e04-269b563a6a3f","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e9ceb376-20bd-444c-880c-38acb2a7a4af","path":"sprites/spr_player_afterimage/spr_player_afterimage.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"615e4190-37a8-483a-bdf6-2ad84b44cd72","Key":9.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"41925b29-015d-42a2-9e8e-e3f7061a65c8","path":"sprites/spr_player_afterimage/spr_player_afterimage.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 25,
    "yorigin": 25,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_player_afterimage","path":"sprites/spr_player_afterimage/spr_player_afterimage.yy",},
    "resourceVersion": "1.4",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"a8ce73b4-26f4-4b7a-91b7-8c68e2633bd8","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "player_sprites",
    "path": "folders/Sprites/player_sprites.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_player_afterimage",
  "tags": [],
  "resourceType": "GMSprite",
}