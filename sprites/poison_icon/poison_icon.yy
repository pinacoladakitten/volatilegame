{
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 59,
  "bbox_top": 0,
  "bbox_bottom": 49,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 60,
  "height": 60,
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"71037127-b120-41ce-9c2c-3d65b73772b2","path":"sprites/poison_icon/poison_icon.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"71037127-b120-41ce-9c2c-3d65b73772b2","path":"sprites/poison_icon/poison_icon.yy",},"LayerId":{"name":"c227b8c1-6682-4e71-88d7-09883814c3c2","path":"sprites/poison_icon/poison_icon.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"poison_icon","path":"sprites/poison_icon/poison_icon.yy",},"resourceVersion":"1.0","name":"71037127-b120-41ce-9c2c-3d65b73772b2","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"5bb8d208-9660-43d7-8f4b-d44a0e0dabe1","path":"sprites/poison_icon/poison_icon.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"5bb8d208-9660-43d7-8f4b-d44a0e0dabe1","path":"sprites/poison_icon/poison_icon.yy",},"LayerId":{"name":"c227b8c1-6682-4e71-88d7-09883814c3c2","path":"sprites/poison_icon/poison_icon.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"poison_icon","path":"sprites/poison_icon/poison_icon.yy",},"resourceVersion":"1.0","name":"5bb8d208-9660-43d7-8f4b-d44a0e0dabe1","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"fdfc1ed1-1a9f-4a55-a0a7-3eda0266ca38","path":"sprites/poison_icon/poison_icon.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"fdfc1ed1-1a9f-4a55-a0a7-3eda0266ca38","path":"sprites/poison_icon/poison_icon.yy",},"LayerId":{"name":"c227b8c1-6682-4e71-88d7-09883814c3c2","path":"sprites/poison_icon/poison_icon.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"poison_icon","path":"sprites/poison_icon/poison_icon.yy",},"resourceVersion":"1.0","name":"fdfc1ed1-1a9f-4a55-a0a7-3eda0266ca38","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"1a6eab3b-2968-45e8-994c-569b888a11d7","path":"sprites/poison_icon/poison_icon.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"1a6eab3b-2968-45e8-994c-569b888a11d7","path":"sprites/poison_icon/poison_icon.yy",},"LayerId":{"name":"c227b8c1-6682-4e71-88d7-09883814c3c2","path":"sprites/poison_icon/poison_icon.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"poison_icon","path":"sprites/poison_icon/poison_icon.yy",},"resourceVersion":"1.0","name":"1a6eab3b-2968-45e8-994c-569b888a11d7","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"af7d94a2-916b-4211-b47c-a1303f3a2fce","path":"sprites/poison_icon/poison_icon.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"af7d94a2-916b-4211-b47c-a1303f3a2fce","path":"sprites/poison_icon/poison_icon.yy",},"LayerId":{"name":"c227b8c1-6682-4e71-88d7-09883814c3c2","path":"sprites/poison_icon/poison_icon.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"poison_icon","path":"sprites/poison_icon/poison_icon.yy",},"resourceVersion":"1.0","name":"af7d94a2-916b-4211-b47c-a1303f3a2fce","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"7ad87c4e-bb16-44b1-83fd-2777f22a0fb7","path":"sprites/poison_icon/poison_icon.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"7ad87c4e-bb16-44b1-83fd-2777f22a0fb7","path":"sprites/poison_icon/poison_icon.yy",},"LayerId":{"name":"c227b8c1-6682-4e71-88d7-09883814c3c2","path":"sprites/poison_icon/poison_icon.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"poison_icon","path":"sprites/poison_icon/poison_icon.yy",},"resourceVersion":"1.0","name":"7ad87c4e-bb16-44b1-83fd-2777f22a0fb7","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"poison_icon","path":"sprites/poison_icon/poison_icon.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 1.0,
    "playbackSpeedType": 1,
    "autoRecord": true,
    "volume": 1.0,
    "length": 6.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"566d64a5-4de6-4cdc-9a6b-8d8a2d566ff6","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"71037127-b120-41ce-9c2c-3d65b73772b2","path":"sprites/poison_icon/poison_icon.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"537c2dff-6fb2-4dc2-9311-d5ffd7e1b2da","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"5bb8d208-9660-43d7-8f4b-d44a0e0dabe1","path":"sprites/poison_icon/poison_icon.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f66911fd-a79c-4a56-8f89-64a862cbbfc1","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"fdfc1ed1-1a9f-4a55-a0a7-3eda0266ca38","path":"sprites/poison_icon/poison_icon.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"cef6c41b-e0e7-4ab7-9977-7e4a126700e4","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"1a6eab3b-2968-45e8-994c-569b888a11d7","path":"sprites/poison_icon/poison_icon.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"afdfdd50-011f-4343-9094-0918eeeb459a","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"af7d94a2-916b-4211-b47c-a1303f3a2fce","path":"sprites/poison_icon/poison_icon.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f7c933bb-54d4-4187-8b70-d8760b6431ec","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"7ad87c4e-bb16-44b1-83fd-2777f22a0fb7","path":"sprites/poison_icon/poison_icon.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 30,
    "yorigin": 30,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"poison_icon","path":"sprites/poison_icon/poison_icon.yy",},
    "resourceVersion": "1.4",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"c227b8c1-6682-4e71-88d7-09883814c3c2","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "poison_form",
    "path": "folders/Sprites/HUD/poison_form.yy",
  },
  "resourceVersion": "1.0",
  "name": "poison_icon",
  "tags": [],
  "resourceType": "GMSprite",
}