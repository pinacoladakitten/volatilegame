{
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 7,
  "bbox_right": 24,
  "bbox_top": 7,
  "bbox_bottom": 24,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 32,
  "height": 32,
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"117caf5d-2522-45ea-987c-4ded24021a87","path":"sprites/spr_item_mana/spr_item_mana.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"117caf5d-2522-45ea-987c-4ded24021a87","path":"sprites/spr_item_mana/spr_item_mana.yy",},"LayerId":{"name":"f5a70420-83dd-4220-8787-4414f6db44de","path":"sprites/spr_item_mana/spr_item_mana.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_item_mana","path":"sprites/spr_item_mana/spr_item_mana.yy",},"resourceVersion":"1.0","name":"117caf5d-2522-45ea-987c-4ded24021a87","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"1d41392a-bb68-43c6-8e69-38c89eb29d2f","path":"sprites/spr_item_mana/spr_item_mana.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"1d41392a-bb68-43c6-8e69-38c89eb29d2f","path":"sprites/spr_item_mana/spr_item_mana.yy",},"LayerId":{"name":"f5a70420-83dd-4220-8787-4414f6db44de","path":"sprites/spr_item_mana/spr_item_mana.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_item_mana","path":"sprites/spr_item_mana/spr_item_mana.yy",},"resourceVersion":"1.0","name":"1d41392a-bb68-43c6-8e69-38c89eb29d2f","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"dea9fd0f-1cd9-4b03-bd40-216084855ce6","path":"sprites/spr_item_mana/spr_item_mana.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"dea9fd0f-1cd9-4b03-bd40-216084855ce6","path":"sprites/spr_item_mana/spr_item_mana.yy",},"LayerId":{"name":"f5a70420-83dd-4220-8787-4414f6db44de","path":"sprites/spr_item_mana/spr_item_mana.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_item_mana","path":"sprites/spr_item_mana/spr_item_mana.yy",},"resourceVersion":"1.0","name":"dea9fd0f-1cd9-4b03-bd40-216084855ce6","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"68a6445f-da88-44bf-afd8-2df0e9f7757e","path":"sprites/spr_item_mana/spr_item_mana.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"68a6445f-da88-44bf-afd8-2df0e9f7757e","path":"sprites/spr_item_mana/spr_item_mana.yy",},"LayerId":{"name":"f5a70420-83dd-4220-8787-4414f6db44de","path":"sprites/spr_item_mana/spr_item_mana.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_item_mana","path":"sprites/spr_item_mana/spr_item_mana.yy",},"resourceVersion":"1.0","name":"68a6445f-da88-44bf-afd8-2df0e9f7757e","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_item_mana","path":"sprites/spr_item_mana/spr_item_mana.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 1.0,
    "playbackSpeedType": 1,
    "autoRecord": true,
    "volume": 1.0,
    "length": 4.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"9db7e9e8-2a12-4cdd-bb08-e2c76c2404e5","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"117caf5d-2522-45ea-987c-4ded24021a87","path":"sprites/spr_item_mana/spr_item_mana.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"412ec4b6-968a-42ea-8de9-6cf5b5288237","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"1d41392a-bb68-43c6-8e69-38c89eb29d2f","path":"sprites/spr_item_mana/spr_item_mana.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"63f23054-ae1a-4455-ae53-2d2058e20600","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"dea9fd0f-1cd9-4b03-bd40-216084855ce6","path":"sprites/spr_item_mana/spr_item_mana.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"29bcb82b-449b-4f1c-ac27-a877202b59e3","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"68a6445f-da88-44bf-afd8-2df0e9f7757e","path":"sprites/spr_item_mana/spr_item_mana.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 16,
    "yorigin": 16,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_item_mana","path":"sprites/spr_item_mana/spr_item_mana.yy",},
    "resourceVersion": "1.4",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"f5a70420-83dd-4220-8787-4414f6db44de","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "items",
    "path": "folders/Sprites/items.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_item_mana",
  "tags": [],
  "resourceType": "GMSprite",
}