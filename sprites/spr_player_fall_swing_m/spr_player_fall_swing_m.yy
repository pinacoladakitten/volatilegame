{
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "bboxMode": 2,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 12,
  "bbox_right": 38,
  "bbox_top": 0,
  "bbox_bottom": 49,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 50,
  "height": 55,
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"1eb3c767-9071-435d-ae95-71d337cdffe2","path":"sprites/spr_player_fall_swing_m/spr_player_fall_swing_m.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"1eb3c767-9071-435d-ae95-71d337cdffe2","path":"sprites/spr_player_fall_swing_m/spr_player_fall_swing_m.yy",},"LayerId":{"name":"75662405-8245-4567-b1bd-18047287fb2d","path":"sprites/spr_player_fall_swing_m/spr_player_fall_swing_m.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_fall_swing_m","path":"sprites/spr_player_fall_swing_m/spr_player_fall_swing_m.yy",},"resourceVersion":"1.0","name":"1eb3c767-9071-435d-ae95-71d337cdffe2","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"5ac5807c-1cfd-49de-beaf-480c3488e4b4","path":"sprites/spr_player_fall_swing_m/spr_player_fall_swing_m.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"5ac5807c-1cfd-49de-beaf-480c3488e4b4","path":"sprites/spr_player_fall_swing_m/spr_player_fall_swing_m.yy",},"LayerId":{"name":"75662405-8245-4567-b1bd-18047287fb2d","path":"sprites/spr_player_fall_swing_m/spr_player_fall_swing_m.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_fall_swing_m","path":"sprites/spr_player_fall_swing_m/spr_player_fall_swing_m.yy",},"resourceVersion":"1.0","name":"5ac5807c-1cfd-49de-beaf-480c3488e4b4","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"52cbb434-33fb-4de9-9efe-b8314e702d38","path":"sprites/spr_player_fall_swing_m/spr_player_fall_swing_m.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"52cbb434-33fb-4de9-9efe-b8314e702d38","path":"sprites/spr_player_fall_swing_m/spr_player_fall_swing_m.yy",},"LayerId":{"name":"75662405-8245-4567-b1bd-18047287fb2d","path":"sprites/spr_player_fall_swing_m/spr_player_fall_swing_m.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_fall_swing_m","path":"sprites/spr_player_fall_swing_m/spr_player_fall_swing_m.yy",},"resourceVersion":"1.0","name":"52cbb434-33fb-4de9-9efe-b8314e702d38","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"6052736a-ad37-43a7-b858-b6d05fe75438","path":"sprites/spr_player_fall_swing_m/spr_player_fall_swing_m.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"6052736a-ad37-43a7-b858-b6d05fe75438","path":"sprites/spr_player_fall_swing_m/spr_player_fall_swing_m.yy",},"LayerId":{"name":"75662405-8245-4567-b1bd-18047287fb2d","path":"sprites/spr_player_fall_swing_m/spr_player_fall_swing_m.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_fall_swing_m","path":"sprites/spr_player_fall_swing_m/spr_player_fall_swing_m.yy",},"resourceVersion":"1.0","name":"6052736a-ad37-43a7-b858-b6d05fe75438","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"b17f2d29-d3b1-4776-ba42-23dda5925f35","path":"sprites/spr_player_fall_swing_m/spr_player_fall_swing_m.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"b17f2d29-d3b1-4776-ba42-23dda5925f35","path":"sprites/spr_player_fall_swing_m/spr_player_fall_swing_m.yy",},"LayerId":{"name":"75662405-8245-4567-b1bd-18047287fb2d","path":"sprites/spr_player_fall_swing_m/spr_player_fall_swing_m.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_fall_swing_m","path":"sprites/spr_player_fall_swing_m/spr_player_fall_swing_m.yy",},"resourceVersion":"1.0","name":"b17f2d29-d3b1-4776-ba42-23dda5925f35","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_player_fall_swing_m","path":"sprites/spr_player_fall_swing_m/spr_player_fall_swing_m.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 1.0,
    "playbackSpeedType": 1,
    "autoRecord": true,
    "volume": 1.0,
    "length": 5.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"55429678-3f73-49c1-8b2e-abb29c024459","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"1eb3c767-9071-435d-ae95-71d337cdffe2","path":"sprites/spr_player_fall_swing_m/spr_player_fall_swing_m.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"77d2526a-5956-404f-9ac0-92165414d694","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"5ac5807c-1cfd-49de-beaf-480c3488e4b4","path":"sprites/spr_player_fall_swing_m/spr_player_fall_swing_m.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a47617ff-eb3c-41bc-ac15-4c3ed707aa46","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"52cbb434-33fb-4de9-9efe-b8314e702d38","path":"sprites/spr_player_fall_swing_m/spr_player_fall_swing_m.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"e63e1666-0539-4702-93d6-4b961556c7de","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"6052736a-ad37-43a7-b858-b6d05fe75438","path":"sprites/spr_player_fall_swing_m/spr_player_fall_swing_m.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d5d63b25-c38a-4220-ae9b-f771aa0ef42c","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"b17f2d29-d3b1-4776-ba42-23dda5925f35","path":"sprites/spr_player_fall_swing_m/spr_player_fall_swing_m.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 25,
    "yorigin": 25,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_player_fall_swing_m","path":"sprites/spr_player_fall_swing_m/spr_player_fall_swing_m.yy",},
    "resourceVersion": "1.4",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"75662405-8245-4567-b1bd-18047287fb2d","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "mirror",
    "path": "folders/Sprites/player_sprites/mirror.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_player_fall_swing_m",
  "tags": [],
  "resourceType": "GMSprite",
}