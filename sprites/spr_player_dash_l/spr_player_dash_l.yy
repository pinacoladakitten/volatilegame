{
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "bboxMode": 2,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 12,
  "bbox_right": 38,
  "bbox_top": 0,
  "bbox_bottom": 49,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 50,
  "height": 60,
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"985ff406-b16f-413c-a827-fcd91a062116","path":"sprites/spr_player_dash_l/spr_player_dash_l.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"985ff406-b16f-413c-a827-fcd91a062116","path":"sprites/spr_player_dash_l/spr_player_dash_l.yy",},"LayerId":{"name":"360d0aca-f596-4b99-aae2-f0d38f7a681c","path":"sprites/spr_player_dash_l/spr_player_dash_l.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_dash_l","path":"sprites/spr_player_dash_l/spr_player_dash_l.yy",},"resourceVersion":"1.0","name":"985ff406-b16f-413c-a827-fcd91a062116","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"1ac69bcc-1c6a-4560-adcb-155f13ed5c84","path":"sprites/spr_player_dash_l/spr_player_dash_l.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"1ac69bcc-1c6a-4560-adcb-155f13ed5c84","path":"sprites/spr_player_dash_l/spr_player_dash_l.yy",},"LayerId":{"name":"360d0aca-f596-4b99-aae2-f0d38f7a681c","path":"sprites/spr_player_dash_l/spr_player_dash_l.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_dash_l","path":"sprites/spr_player_dash_l/spr_player_dash_l.yy",},"resourceVersion":"1.0","name":"1ac69bcc-1c6a-4560-adcb-155f13ed5c84","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"dff63411-ce02-4d95-b29e-581dc08bb152","path":"sprites/spr_player_dash_l/spr_player_dash_l.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"dff63411-ce02-4d95-b29e-581dc08bb152","path":"sprites/spr_player_dash_l/spr_player_dash_l.yy",},"LayerId":{"name":"360d0aca-f596-4b99-aae2-f0d38f7a681c","path":"sprites/spr_player_dash_l/spr_player_dash_l.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_dash_l","path":"sprites/spr_player_dash_l/spr_player_dash_l.yy",},"resourceVersion":"1.0","name":"dff63411-ce02-4d95-b29e-581dc08bb152","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"959cc606-f3b0-4fbd-99ae-d29b2ad8fbf1","path":"sprites/spr_player_dash_l/spr_player_dash_l.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"959cc606-f3b0-4fbd-99ae-d29b2ad8fbf1","path":"sprites/spr_player_dash_l/spr_player_dash_l.yy",},"LayerId":{"name":"360d0aca-f596-4b99-aae2-f0d38f7a681c","path":"sprites/spr_player_dash_l/spr_player_dash_l.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_dash_l","path":"sprites/spr_player_dash_l/spr_player_dash_l.yy",},"resourceVersion":"1.0","name":"959cc606-f3b0-4fbd-99ae-d29b2ad8fbf1","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"968c6bed-00e9-4546-b41f-f72a2cc38516","path":"sprites/spr_player_dash_l/spr_player_dash_l.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"968c6bed-00e9-4546-b41f-f72a2cc38516","path":"sprites/spr_player_dash_l/spr_player_dash_l.yy",},"LayerId":{"name":"360d0aca-f596-4b99-aae2-f0d38f7a681c","path":"sprites/spr_player_dash_l/spr_player_dash_l.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_dash_l","path":"sprites/spr_player_dash_l/spr_player_dash_l.yy",},"resourceVersion":"1.0","name":"968c6bed-00e9-4546-b41f-f72a2cc38516","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"a2b522b5-d7c8-400c-8413-bda7d3659941","path":"sprites/spr_player_dash_l/spr_player_dash_l.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a2b522b5-d7c8-400c-8413-bda7d3659941","path":"sprites/spr_player_dash_l/spr_player_dash_l.yy",},"LayerId":{"name":"360d0aca-f596-4b99-aae2-f0d38f7a681c","path":"sprites/spr_player_dash_l/spr_player_dash_l.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_dash_l","path":"sprites/spr_player_dash_l/spr_player_dash_l.yy",},"resourceVersion":"1.0","name":"a2b522b5-d7c8-400c-8413-bda7d3659941","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"630f766a-55d2-4afb-a147-fa9ba1aaf9a7","path":"sprites/spr_player_dash_l/spr_player_dash_l.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"630f766a-55d2-4afb-a147-fa9ba1aaf9a7","path":"sprites/spr_player_dash_l/spr_player_dash_l.yy",},"LayerId":{"name":"360d0aca-f596-4b99-aae2-f0d38f7a681c","path":"sprites/spr_player_dash_l/spr_player_dash_l.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_dash_l","path":"sprites/spr_player_dash_l/spr_player_dash_l.yy",},"resourceVersion":"1.0","name":"630f766a-55d2-4afb-a147-fa9ba1aaf9a7","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"a8f6b19d-6aea-4cbe-b387-dc03670421f0","path":"sprites/spr_player_dash_l/spr_player_dash_l.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a8f6b19d-6aea-4cbe-b387-dc03670421f0","path":"sprites/spr_player_dash_l/spr_player_dash_l.yy",},"LayerId":{"name":"360d0aca-f596-4b99-aae2-f0d38f7a681c","path":"sprites/spr_player_dash_l/spr_player_dash_l.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_dash_l","path":"sprites/spr_player_dash_l/spr_player_dash_l.yy",},"resourceVersion":"1.0","name":"a8f6b19d-6aea-4cbe-b387-dc03670421f0","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_player_dash_l","path":"sprites/spr_player_dash_l/spr_player_dash_l.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 1.0,
    "playbackSpeedType": 1,
    "autoRecord": true,
    "volume": 1.0,
    "length": 8.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"4612e1e8-cafe-47bd-a688-5dd1be9346d4","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"985ff406-b16f-413c-a827-fcd91a062116","path":"sprites/spr_player_dash_l/spr_player_dash_l.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"9f8094f0-c46b-4fd7-a513-4b2b7e282969","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"1ac69bcc-1c6a-4560-adcb-155f13ed5c84","path":"sprites/spr_player_dash_l/spr_player_dash_l.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"237125cd-e299-4406-9c88-4c036d0accb9","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"dff63411-ce02-4d95-b29e-581dc08bb152","path":"sprites/spr_player_dash_l/spr_player_dash_l.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c1128850-2c15-4681-a959-5fa4801790cf","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"959cc606-f3b0-4fbd-99ae-d29b2ad8fbf1","path":"sprites/spr_player_dash_l/spr_player_dash_l.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"5cb05daf-06af-442a-930a-1bb512824139","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"968c6bed-00e9-4546-b41f-f72a2cc38516","path":"sprites/spr_player_dash_l/spr_player_dash_l.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"dbc211bf-04f9-4416-a8bb-b405bf903890","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a2b522b5-d7c8-400c-8413-bda7d3659941","path":"sprites/spr_player_dash_l/spr_player_dash_l.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"fdc4bf96-def8-4fae-970e-579f3c3116ca","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"630f766a-55d2-4afb-a147-fa9ba1aaf9a7","path":"sprites/spr_player_dash_l/spr_player_dash_l.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"b274fb2e-3cfc-4636-b85d-ca3ab7237f16","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a8f6b19d-6aea-4cbe-b387-dc03670421f0","path":"sprites/spr_player_dash_l/spr_player_dash_l.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 25,
    "yorigin": 25,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_player_dash_l","path":"sprites/spr_player_dash_l/spr_player_dash_l.yy",},
    "resourceVersion": "1.4",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"360d0aca-f596-4b99-aae2-f0d38f7a681c","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "lightning",
    "path": "folders/Sprites/player_sprites/lightning.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_player_dash_l",
  "tags": [],
  "resourceType": "GMSprite",
}