{
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 47,
  "bbox_top": 0,
  "bbox_bottom": 39,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 48,
  "height": 40,
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"86c19a1a-036c-4c2f-be63-ad9ffa08ebcc","path":"sprites/spr_spellp01/spr_spellp01.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"86c19a1a-036c-4c2f-be63-ad9ffa08ebcc","path":"sprites/spr_spellp01/spr_spellp01.yy",},"LayerId":{"name":"b6e42aad-72ab-4f0f-92d2-8b40a07c9ff5","path":"sprites/spr_spellp01/spr_spellp01.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_spellp01","path":"sprites/spr_spellp01/spr_spellp01.yy",},"resourceVersion":"1.0","name":"86c19a1a-036c-4c2f-be63-ad9ffa08ebcc","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"721e6ed7-83d1-4393-aa3e-88bbfd4006ca","path":"sprites/spr_spellp01/spr_spellp01.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"721e6ed7-83d1-4393-aa3e-88bbfd4006ca","path":"sprites/spr_spellp01/spr_spellp01.yy",},"LayerId":{"name":"b6e42aad-72ab-4f0f-92d2-8b40a07c9ff5","path":"sprites/spr_spellp01/spr_spellp01.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_spellp01","path":"sprites/spr_spellp01/spr_spellp01.yy",},"resourceVersion":"1.0","name":"721e6ed7-83d1-4393-aa3e-88bbfd4006ca","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_spellp01","path":"sprites/spr_spellp01/spr_spellp01.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 1.0,
    "playbackSpeedType": 1,
    "autoRecord": true,
    "volume": 1.0,
    "length": 2.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"1e46c9eb-e804-4c2b-80b2-b06e5ee87702","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"86c19a1a-036c-4c2f-be63-ad9ffa08ebcc","path":"sprites/spr_spellp01/spr_spellp01.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"9416ec03-c3c5-4e42-9a1b-4750088ad333","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"721e6ed7-83d1-4393-aa3e-88bbfd4006ca","path":"sprites/spr_spellp01/spr_spellp01.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 19,
    "yorigin": 19,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_spellp01","path":"sprites/spr_spellp01/spr_spellp01.yy",},
    "resourceVersion": "1.4",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"b6e42aad-72ab-4f0f-92d2-8b40a07c9ff5","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "poison_form",
    "path": "folders/Sprites/HUD/poison_form.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_spellp01",
  "tags": [],
  "resourceType": "GMSprite",
}