{
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "bboxMode": 2,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 12,
  "bbox_right": 38,
  "bbox_top": 1,
  "bbox_bottom": 49,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 50,
  "height": 50,
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"3a0e2686-c5eb-45de-9b12-1b8b44d97352","path":"sprites/spr_player_shoot_a/spr_player_shoot_a.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"3a0e2686-c5eb-45de-9b12-1b8b44d97352","path":"sprites/spr_player_shoot_a/spr_player_shoot_a.yy",},"LayerId":{"name":"86b3ec8d-071f-4e66-9717-27e7e17371ca","path":"sprites/spr_player_shoot_a/spr_player_shoot_a.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_shoot_a","path":"sprites/spr_player_shoot_a/spr_player_shoot_a.yy",},"resourceVersion":"1.0","name":"3a0e2686-c5eb-45de-9b12-1b8b44d97352","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"0639d270-4dbc-46d9-9a7a-c35d896b2e02","path":"sprites/spr_player_shoot_a/spr_player_shoot_a.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"0639d270-4dbc-46d9-9a7a-c35d896b2e02","path":"sprites/spr_player_shoot_a/spr_player_shoot_a.yy",},"LayerId":{"name":"86b3ec8d-071f-4e66-9717-27e7e17371ca","path":"sprites/spr_player_shoot_a/spr_player_shoot_a.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_shoot_a","path":"sprites/spr_player_shoot_a/spr_player_shoot_a.yy",},"resourceVersion":"1.0","name":"0639d270-4dbc-46d9-9a7a-c35d896b2e02","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"1d4c2910-d625-4d28-be25-3fbf7e5c1d7d","path":"sprites/spr_player_shoot_a/spr_player_shoot_a.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"1d4c2910-d625-4d28-be25-3fbf7e5c1d7d","path":"sprites/spr_player_shoot_a/spr_player_shoot_a.yy",},"LayerId":{"name":"86b3ec8d-071f-4e66-9717-27e7e17371ca","path":"sprites/spr_player_shoot_a/spr_player_shoot_a.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_shoot_a","path":"sprites/spr_player_shoot_a/spr_player_shoot_a.yy",},"resourceVersion":"1.0","name":"1d4c2910-d625-4d28-be25-3fbf7e5c1d7d","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"8bc66f4b-fe47-423a-9c29-df4743924b98","path":"sprites/spr_player_shoot_a/spr_player_shoot_a.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"8bc66f4b-fe47-423a-9c29-df4743924b98","path":"sprites/spr_player_shoot_a/spr_player_shoot_a.yy",},"LayerId":{"name":"86b3ec8d-071f-4e66-9717-27e7e17371ca","path":"sprites/spr_player_shoot_a/spr_player_shoot_a.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_shoot_a","path":"sprites/spr_player_shoot_a/spr_player_shoot_a.yy",},"resourceVersion":"1.0","name":"8bc66f4b-fe47-423a-9c29-df4743924b98","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"410124ea-5b03-4f4b-aecc-a2b569eed024","path":"sprites/spr_player_shoot_a/spr_player_shoot_a.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"410124ea-5b03-4f4b-aecc-a2b569eed024","path":"sprites/spr_player_shoot_a/spr_player_shoot_a.yy",},"LayerId":{"name":"86b3ec8d-071f-4e66-9717-27e7e17371ca","path":"sprites/spr_player_shoot_a/spr_player_shoot_a.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_shoot_a","path":"sprites/spr_player_shoot_a/spr_player_shoot_a.yy",},"resourceVersion":"1.0","name":"410124ea-5b03-4f4b-aecc-a2b569eed024","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_player_shoot_a","path":"sprites/spr_player_shoot_a/spr_player_shoot_a.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 1.0,
    "playbackSpeedType": 1,
    "autoRecord": true,
    "volume": 1.0,
    "length": 5.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"490004e0-3831-4f46-8f26-65d578b7a9ff","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"3a0e2686-c5eb-45de-9b12-1b8b44d97352","path":"sprites/spr_player_shoot_a/spr_player_shoot_a.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"02799177-3c28-42f5-bd7c-e1d62389a8e8","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"0639d270-4dbc-46d9-9a7a-c35d896b2e02","path":"sprites/spr_player_shoot_a/spr_player_shoot_a.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"8110d5d2-f9c8-409b-9ff7-bf72934f2579","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"1d4c2910-d625-4d28-be25-3fbf7e5c1d7d","path":"sprites/spr_player_shoot_a/spr_player_shoot_a.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"b0e46a4a-58d4-45af-9353-2771e2a7d321","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"8bc66f4b-fe47-423a-9c29-df4743924b98","path":"sprites/spr_player_shoot_a/spr_player_shoot_a.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f6fc8812-9f6d-4fc9-b2c0-9dfcb37c0356","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"410124ea-5b03-4f4b-aecc-a2b569eed024","path":"sprites/spr_player_shoot_a/spr_player_shoot_a.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 25,
    "yorigin": 25,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_player_shoot_a","path":"sprites/spr_player_shoot_a/spr_player_shoot_a.yy",},
    "resourceVersion": "1.4",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"86b3ec8d-071f-4e66-9717-27e7e17371ca","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "astral",
    "path": "folders/Sprites/player_sprites/astral.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_player_shoot_a",
  "tags": [],
  "resourceType": "GMSprite",
}