/// @description Background Scrolling
__background_set( e__BG.X, 0, __view_get( e__VW.XView, 0 ) * 1 )
__background_set( e__BG.X, 1, __view_get( e__VW.XView, 0 ) * 1 )
__background_set( e__BG.X, 4, __view_get( e__VW.XView, 0 ) * 0.295 )
__background_set( e__BG.X, 5, __view_get( e__VW.XView, 0 ) * 0.25 )
__background_set( e__BG.HSpeed, 2, obj_player.hsp+0.05 )
__background_set( e__BG.HSpeed, 3, obj_player.hsp+0.02 )

