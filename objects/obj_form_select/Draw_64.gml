/// @description Drawing Everything
xx = 1024 * 0.4
yy = 576 - 60
/*if 1 = 2
{
    xx = (window_get_width() * 0.5) - 256
    yy = window_get_height() - 120
}*/
//Form HUD
if global.form = 1
{
    draw_sprite_ext(spr_fire_icon_tab,0,xx+0,yy+0,1,1,0,-1,1)
    draw_sprite_ext(spr_poison_icon_tab,1,xx+42*1,yy+0,1,1,0,-1,1)
    draw_sprite_ext(spr_astral_icon_tab,1,xx+84*1,yy+0,1,1,0,-1,1)
    draw_sprite_ext(spr_lightning_icon_tab,1,xx+128*1,yy+0,1,1,0,-1,1)
    draw_sprite_ext(spr_mirror_icon_tab,1,xx+170*1,yy+0,1,1,0,-1,1)
    draw_sprite_ext(spr_mirror_icon_tab,2,xx+212*1,yy+0,1,1,0,-1,1)
}
if global.form = 2
{
    draw_sprite_ext(spr_fire_icon_tab,1,xx+0,yy+0,1,1,0,-1,1)
    draw_sprite_ext(spr_poison_icon_tab,0,xx+42*1,yy+0,1,1,0,-1,1)
    draw_sprite_ext(spr_astral_icon_tab,1,xx+84*1,yy+0,1,1,0,-1,1)
    draw_sprite_ext(spr_lightning_icon_tab,1,xx+128*1,yy+0,1,1,0,-1,1)
    draw_sprite_ext(spr_mirror_icon_tab,1,xx+170*1,yy+0,1,1,0,-1,1)
    draw_sprite_ext(spr_mirror_icon_tab,2,xx+212*1,yy+0,1,1,0,-1,1)
}
if global.form = 3
{
    draw_sprite_ext(spr_fire_icon_tab,1,xx+0,yy+0,1,1,0,-1,1)
    draw_sprite_ext(spr_poison_icon_tab,1,xx+42*1,yy+0,1,1,0,-1,1)
    draw_sprite_ext(spr_astral_icon_tab,0,xx+84*1,yy+0,1,1,0,-1,1)
    draw_sprite_ext(spr_lightning_icon_tab,1,xx+128*1,yy+0,1,1,0,-1,1)
    draw_sprite_ext(spr_mirror_icon_tab,1,xx+170*1,yy+0,1,1,0,-1,1)
    draw_sprite_ext(spr_mirror_icon_tab,2,xx+212*1,yy+0,1,1,0,-1,1)
}
if global.form = 4
{
    draw_sprite_ext(spr_fire_icon_tab,1,xx+0,yy+0,1,1,0,-1,1)
    draw_sprite_ext(spr_poison_icon_tab,1,xx+42*1,yy+0,1,1,0,-1,1)
    draw_sprite_ext(spr_astral_icon_tab,1,xx+84*1,yy+0,1,1,0,-1,1)
    draw_sprite_ext(spr_lightning_icon_tab,0,xx+128*1,yy+0,1,1,0,-1,1)
    draw_sprite_ext(spr_mirror_icon_tab,1,xx+170*1,yy+0,1,1,0,-1,1)
    draw_sprite_ext(spr_mirror_icon_tab,2,xx+212*1,yy+0,1,1,0,-1,1)
}
if global.form = 5
{
    draw_sprite_ext(spr_fire_icon_tab,1,xx+0,yy+0,1,1,0,-1,1)
    draw_sprite_ext(spr_poison_icon_tab,1,xx+42*1,yy+0,1,1,0,-1,1)
    draw_sprite_ext(spr_astral_icon_tab,1,xx+84*1,yy+0,1,1,0,-1,1)
    draw_sprite_ext(spr_lightning_icon_tab,1,xx+128*1,yy+0,1,1,0,-1,1)
    draw_sprite_ext(spr_mirror_icon_tab,0,xx+170*1,yy+0,1,1,0,-1,1)
    draw_sprite_ext(spr_mirror_icon_tab,2,xx+212*1,yy+0,1,1,0,-1,1)
}


/* */
/*  */
