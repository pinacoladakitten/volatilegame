/// @description Fade Effect
if fade = 0
{
    image_alpha = min(image_alpha+0.01,1)
}

if image_alpha = 1
{
    fade = 1
}

if fade = 1
{
    image_alpha = max(0,image_alpha-0.01)
}

///Room Name
if room == test
{
    sprite_index = Test_stage
}
if room == room01_start
{
    sprite_index = Ruined_castle_title
}

action_move_to(__view_get( e__VW.XView, 0 ), __view_get( e__VW.YView, 0 ));
