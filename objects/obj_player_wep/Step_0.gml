image_speed = 0.5

with (obj_player) {
var __b__;
__b__ = action_if_variable(image_xscale, 1, 0);
}
if __b__
{
action_move_to(obj_player.x+25, obj_player.y);
}
with (obj_player) {
__b__ = action_if_variable(image_xscale, -1, 0);
}
if __b__
{
action_move_to(obj_player.x-25, obj_player.y);
}
with (obj_player) {
__b__ = action_if_variable(fire_form, 1, 0);
}
if __b__
{
sprite_index = spr_player_wep
global.meleedamage = random_range(4, 7)

}
with (obj_player) {
__b__ = action_if_variable(poison_form, 1, 0);
}
if __b__
{
sprite_index = spr_player_wep_p
global.meleedamage = random_range(2, 4)

}
with (obj_player) {
__b__ = action_if_variable(astral_form, 1, 0);
}
if __b__
{
sprite_index = spr_player_wep_a

}
with (obj_player) {
__b__ = action_if_variable(lightning_form, 1, 0);
}
if __b__
{
sprite_index = spr_player_wep_l

}
with (obj_player) {
__b__ = action_if_variable(mirror_form, 1, 0);
}
if __b__
{
sprite_index = spr_player_wep_m
global.meleedamage = random_range(2, 5)

}
