/// @description Collision stuff

//Horizontal Collision
if (place_meeting(x+hsp,y,obj_col))
{
    while(!place_meeting(x+sign(hsp),y,obj_col))
    {
    x += sign(hsp);
    }
    hsp = 0;
}
//Vertical Collision
if (place_meeting(x,y+vsp,obj_col))
{
    while(!place_meeting(x,y+sign(vsp),obj_col))
    {
    y += sign(vsp);
    }
    vsp = 0;
}


x += hsp;
y += vsp;

if (vsp < 5) vsp += grav;
hsp = random_range(-10,10)
vsp = random_range(-10,10)

