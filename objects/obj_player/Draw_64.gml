/// @description HUD
//Health Bar
draw_healthbar(54, 50, 332, 80, mana, c_black, c_navy, c_navy, 0, false, true)
draw_healthbar(54, 25, 332, 45, life, c_black, c_maroon, c_maroon, 0, false, true)

//Rest of the HUD
draw_sprite(icon_thing,-1,0,0)
draw_sprite(spr_form_plate,-1,30,50)
if (fire_form = 1)
{
    draw_sprite(fire_icon,-1,30,50)
    if (firemag1 = 1)
    {
        draw_sprite(spr_spellf01,0,25,105)
        draw_sprite(spr_spellf02,1,25,150)
    }
    if (firemag2 = 1)
    {
        draw_sprite(spr_spellf01,1,25,105)
        draw_sprite(spr_spellf02,0,25,150)
    }
}
if (poison_form = 1)
{
    draw_sprite(poison_icon,-1,30,50)
    if (poisonmag1 = 1)
    {
        draw_sprite(spr_spellp01,0,25,105)
        draw_sprite(spr_spellp02,1,25,150)
    }
    if (poisonmag2 = 1)
    {
        draw_sprite(spr_spellp01,1,25,105)
        draw_sprite(spr_spellp02,0,25,150)
    }
}
if (mirror_form = 1)
{
    draw_sprite(mirror_icon,-1,30,50)
    if (mirrormag1 = 1)
    {
        draw_sprite(spr_spellm01,0,25,105)
        draw_sprite(spr_spellm02,1,25,150)
    }
    if (mirrormag2 = 1)
    {
        draw_sprite(spr_spellm01,1,25,105)
        draw_sprite(spr_spellm02,0,25,150)
    }
}
if (lightning_form = 1)
{
    draw_sprite(lightning_icon,-1,30,50)
    if (lightningmag1 = 1)
    {
        draw_sprite(spr_spelll01,0,25,105)
        draw_sprite(spr_spelll02,1,25,150)
    }
    if (lightningmag2 = 1)
    {
        draw_sprite(spr_spelll01,1,25,105)
        draw_sprite(spr_spelll02,0,25,150)
    }
}





