/// @description Hit by an enemy
if invincible = 0
if hurt = 0
{
audio_play_sound(snd_plhurt, 10, false);
life = life - 8
hurt = 1
invincible = 1
if (fire_form = 1)sprite_index = spr_player_hurt
if (poison_form = 1)sprite_index = spr_player_hurt_p
if (astral_form = 1)sprite_index = spr_player_hurt_a
if (lightning_form = 1)sprite_index = spr_player_hurt_l
if (mirror_form = 1)sprite_index = spr_player_hurt_m
image_alpha = 0.5
alarm[0] = room_speed * 2;
alarm[1] = room_speed * 0.5;
}

