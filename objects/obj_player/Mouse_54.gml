/// @description Magic Attacks

//Form Fire
//Fireball (Spell 1)
if fire_form = 1
if firemag1 = 1
{
    if mana > 20
    {
        mana = mana - 20
        attack = 1
        if (fire_form = 1) sprite_index = spr_player_shoot
        if (poison_form = 1) sprite_index = spr_player_shoot_p
        if (astral_form = 1) sprite_index = spr_player_shoot_a
        audio_play_sound(snd_fireball, 10, false);
        instance_create(obj_player.x ,obj_player.y ,obj_fireball_mag)
    }
}
//Sunball (Spell 2)
if fire_form = 1
if firemag2 = 1
{
    if mana > 33
    {
        mana = mana - 33
        attack = 1
        if (fire_form = 1) sprite_index = spr_player_shoot
        if (poison_form = 1) sprite_index = spr_player_shoot_p
        if (astral_form = 1) sprite_index = spr_player_shoot_a
        audio_play_sound(snd_fireball, 10, false);
        instance_create(obj_player.x ,obj_player.y ,obj_starball_mag)
    }
}

//Form poison----------------------------------------------------
//Corrupted Seed (Spell 1)
if poison_form = 1
if poisonmag1 = 1
{
    if instance_number(obj_seed_mag) < 2
    if mana > 10
    {
        mana = mana - 10
        attack = 1
        if (fire_form = 1) sprite_index = spr_player_shoot
        if (poison_form = 1) sprite_index = spr_player_shoot_p
        if (astral_form = 1) sprite_index = spr_player_shoot_a
        audio_play_sound(snd_seed, 10, false);
        instance_create(obj_player.x ,obj_player.y ,obj_seed_mag)
    }
}
//(Spell 2)
if poison_form = 1
if poisonmag2 = 1
{
    if instance_number(obj_poisonball) < 1
    if mana > 10
    {
        mana = mana - 10
        audio_play_sound(snd_seed, 10, false);
        instance_create(x,y,obj_poisonball)
        poisonball = 1
    }
}

//Form Astral--------------------------------------------------------------
//Blades of Stardust (Spell 1)
if astral_form = 1
if astralmag1 = 1
{
    if instance_number(obj_starblade_mag) < 2
    if mana > 20
    {
        attack = 1
        audio_play_sound(snd_seed, 10, false);
        instance_create(obj_player.x ,obj_player.y ,obj_starblade_mag)
        if (fire_form = 1) sprite_index = spr_player_shoot
        if (poison_form = 1) sprite_index = spr_player_shoot_p
        if (astral_form = 1) sprite_index = spr_player_shoot_a
    }
}
//(Spell 2)

//Form Lightning----------------------------------------------------------------------
//Lightning (Spell 1)
if lightning_form = 1
if lightningmag1 = 1
{
    if sprite_index != spr_player_shoot_l
    if mana > 15
    {
        mana = mana - 15
        attack = 1
        sattack = 1
        if (fire_form = 1) sprite_index = spr_player_shoot
        if (poison_form = 1) sprite_index = spr_player_shoot_p
        if (astral_form = 1) sprite_index = spr_player_shoot_a
        if (lightning_form = 1) sprite_index = spr_player_shoot_l
        audio_play_sound(snd_lightdash, 10, false);
        hsp = hsp + 7*sign(hsp)
        invincible = 1
        alarm[1] = room_speed * 0.3;
        alarm[0] = room_speed * 0.3;
    }
}

//Form Fire
//Illusion step (Spell 1)-----------------------------------------------------
if mirror_form = 1
if mirrormag1 = 1
{
    if instance_number(obj_player_mirror) < 1
    if mana > 15
    {
        mana = mana - 15
        audio_play_sound(snd_seed, 10, false);
        instance_create(x ,y ,obj_player_mirror)
    }
}

