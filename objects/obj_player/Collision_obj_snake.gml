/// @description Hit by an enemy
if invincible = 0
if hurt = 0
{
audio_play_sound(snd_plhurt, 10, false);
life = life - 3
hurt = 1
if (fire_form = 1)sprite_index = spr_player_hurt
if (poison_form = 1)sprite_index = spr_player_hurt_p
if (astral_form = 1)sprite_index = spr_player_hurt_a
if (lightning_form = 1)sprite_index = spr_player_hurt_l
if (mirror_form = 1)sprite_index = spr_player_hurt_m
image_alpha = 0.5
alarm[0] = room_speed * 2;
alarm[1] = room_speed * 0.5;
alarm[2] = room_speed * 10;
}

var __b__;
__b__ = action_if_variable(invincible, 0, 0);
if __b__
{
__b__ = action_if_dice(3);
if __b__
{
image_blend = c_purple
if poison_form = 1
{
poisoned = 0.015
}
else
{
poisoned = 0.03
}

}
}
invincible = 1;
