/// @description Defining Inputs
grav = 0.25;
hsp = 0;
vsp = 0;
jumpspeed = 5;
movespeed = 3;
doublej = 1;
attack = 0;
mana = 100;
life = 100;
invincible = 0;
hurt = 0;
poisoned = 0;
dash = 1;
stamina = 1;
spellnumber = 1;
subhsp = 1;
//Form Fire
fire_form = 0;
firemag1 = 0;
firemag2 = 0;
//Form Poison
poison_form = 0;
poisonmag1 = 0;
poisonmag2 = 0;
poisonball = 0
//Form Astral
astral_form = 0;
astralmag1 = 0;
astralmag2 = 0;
//Form Lightning
lightning_form = 0;
lightningmag1 = 0;
lightningmag2 = 0;
sattack = 0;
//Form Mirror
mirror_form = 0;
mirrormag1 = 0;
mirrormag2 = 0;
sattack = 0;

///Forms
if global.form = 1
{
    fire_form = 1
    firemag1 = 1
    firemag2 = 0
}
if global.form = 2
{
    poison_form = 1
    poisonmag1 = 1
}
if global.form = 3
{
    astral_form = 1
    astralmag1 = 1
}
if global.form = 4
{
    lightning_form = 1
    lightningmag1 = 1
}
if global.form = 5
{
    mirror_form = 1
    mirrormag1 = 1
}

///Create global stats
instance_create(0,0,player_stats)
instance_create(x,y,obj_form_select)

