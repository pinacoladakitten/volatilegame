/// @description Getting Inputs
if (place_meeting(x,y+1,obj_col))
{
key_right = keyboard_check(ord("D")) * 0.3;
key_left = -keyboard_check(ord("A")) * 0.3;
}
if !(place_meeting(x,y+1,obj_col))
{
key_right = keyboard_check(ord("D")) * 0.2;
key_left = -keyboard_check(ord("A")) * 0.2;
}
key_jump = keyboard_check_pressed(vk_space);

///Reaction to inputs
if (hurt = 0)
if (sattack = 0)
{
move = key_left + key_right;
if (hsp >= 0){
hsp = min(hsp+move,movespeed);
}
if (hsp <= 0) {
hsp = max(-1*movespeed,hsp+move);
}
if move = 0
{
    if (place_meeting(x,y+1,obj_col))
    {
        if (hsp > 0)
        {
        hsp = max(0,hsp-0.3)
        }
        if (hsp < 0) 
        {
        hsp = min(hsp+0.3,0)
        }
    }
    if (!place_meeting(x,y+1,obj_col))
    {
        if (hsp > 0)
        {
        hsp = hsp-0.02
        }
        if (hsp < 0)
        {
        hsp = hsp+0.02
        }
    }

}

}
if (hsp = 0){
movespeed = 3
}

if mouse_wheel_down()
{
if spellnumber = 1
spellnumber = 2
}
if mouse_wheel_up()
{
if spellnumber = 2
spellnumber = 1
}

//Gravity, Jumping, and Double Jumping
if (vsp < 5) vsp += grav;
if (dash = 1)
    {
if (place_meeting(x,y+1,obj_col))
{
        doublej = 1;
        vsp = key_jump * -jumpspeed
}
if (!place_meeting(x,y+1,obj_col))
{
if (doublej = 1)
if (keyboard_check_pressed(vk_space))
{
instance_create(obj_player.x,obj_player.y+15,obj_eff_doublej)
vsp = key_jump * -jumpspeed
doublej = 0;
}
    }
}

///Collision stuff

//Horizontal Collision
if (place_meeting(x+hsp,y,obj_col))
{
    while(!place_meeting(x+sign(hsp),y,obj_col))
    {
    x += sign(round(hsp));
    }
    hsp = 0;
}
x += round(hsp);
//Vertical Collision
if (place_meeting(x,y+vsp,obj_col))
{
    while(!place_meeting(x,y+sign(vsp),obj_col))
    {
    y += sign(vsp);
    }
    vsp = 0;
}
y += vsp;

if (hurt = 1)
{
move_bounce_solid(false)
}

if (sattack = 1)
{
move_bounce_solid(true)
}

///Sprites
image_speed=0.25

if (image_index = 3) 
if (attack = 1) 
attack = 0;

if attack = 0
if hurt = 0
if poisonball = 0
{
    if (hsp > 0) image_xscale = 1
    if (hsp < 0) image_xscale = -1

    if (place_meeting(x,y+1,obj_col))
    if sattack = 0
    {
        if (hsp = 0) 
        {
            if (fire_form = 1) sprite_index = spr_player_idle
            if (poison_form = 1) sprite_index = spr_player_idle_p
            if (astral_form = 1) sprite_index = spr_player_idle_a
            if (lightning_form = 1) sprite_index = spr_player_idle_l
             if (mirror_form = 1) sprite_index = spr_player_idle_m
        }
        if (hsp > 0 or hsp < 0)
        {
            if (fire_form = 1) sprite_index = spr_player_walk
            if (poison_form = 1) sprite_index = spr_player_walk_p
            if (astral_form = 1) sprite_index = spr_player_walk_a
            if (lightning_form = 1) sprite_index = spr_player_walk_l
            if (mirror_form = 1) sprite_index = spr_player_walk_m
        }
    }
    if (vsp < 1)
    if (!place_meeting(x,y+1,obj_col))
    if sattack = 0
    {
        if (fire_form = 1) sprite_index = spr_player_jump
        if (poison_form = 1) sprite_index = spr_player_jump_p
        if (astral_form = 1) sprite_index = spr_player_jump_a
        if (lightning_form = 1) sprite_index = spr_player_jump_l
        if (mirror_form = 1) sprite_index = spr_player_jump_m
    }
    if (vsp > 1)
    if (!place_meeting(x,y+1,obj_col))
    {
     if (fire_form = 1) sprite_index = spr_player_fall
     if (poison_form = 1) sprite_index = spr_player_fall_p
     if (astral_form = 1) sprite_index = spr_player_fall_a
     if (lightning_form = 1) sprite_index = spr_player_fall_l
     if (mirror_form = 1) sprite_index = spr_player_fall_m
    }
    if dash = 0 {
    if (fire_form = 1) sprite_index = spr_player_dash
    if (poison_form = 1) sprite_index = spr_player_dash_p
    if (lightning_form = 1) sprite_index = spr_player_dash_l
    if (mirror_form = 1) sprite_index = spr_player_dash_m
    }
}
if poisonball = 1
if poison_form = 1
{
sprite_index = spr_invisible
}

///Stuff occurring

//Mana regen
if (mana < 100)
{
mana = mana + 0.1
if (astral_form = 1) mana = mana + 0.2
}

//Activation of Objects
{
  instance_activate_all();
  instance_deactivate_region(__view_get( e__VW.XView, 0 ),__view_get( e__VW.YView, 0 ),
                        __view_get( e__VW.WView, 0 ),__view_get( e__VW.HView, 0 ),false,true);
}
instance_activate_object(obj_col);

//Poisoned status
if (poisoned > 0)
{
life = life - poisoned
}
//Afterimage
if dash = 0 {
instance_create(x,y,obj_afterimage)
}

//Mana drainage spells
if poisonball = 1
if poison_form = 1
{
mana = max(0,mana-0.2)
if (mana = 0)
    {
    poisonball = 0
    with (obj_poisonball)
        {
        instance_destroy();
        }
    }
}

///Forms

//Fire (default)
if keyboard_check_pressed(ord("Z"))
{
fire_form = 1
firemag1 = 1
firemag2 = 0
global.form = 1
}
//magic selection (spell 1 or 2)
if (fire_form = 1) 
    {
    if spellnumber = 1
        {
        firemag1 = 1
        firemag2 = 0
        }
    if spellnumber = 2
        {
        firemag2 = 1
        firemag1 = 0
        }
    }
//fire form check
if fire_form = 1
{
astral_form = 0
poison_form = 0
lightning_form = 0
mirror_form = 0
}

//Poison
if keyboard_check_pressed(ord("X"))
{
poison_form = 1
poisonmag1 = 1
poisonmag2 = 0
global.form = 2
}
//magic selection (spell 1 or 2)
if poison_form = 1
    {
    if spellnumber = 1
        {
        poisonmag1 = 1
        poisonmag2 = 0
        }
    if spellnumber = 2
        {
        poisonmag2 = 1
        poisonmag1 = 0
        }
    }
//poison form check
if poison_form = 1
{
fire_form = 0
astral_form = 0
lightning_form = 0
mirror_form = 0
}
//Poison ball
if (poison_form = 0) or (poisonmag1 = 1)
{
poisonball = 0
with (obj_poisonball)
    {
    instance_destroy();
    }
}
//Astral
if keyboard_check_pressed(ord("C"))
{
astral_form = 1
astralmag1 = 1
global.form = 3
}
if astral_form = 1
{
fire_form = 0
poison_form = 0
lightning_form = 0
mirror_form = 0
}

//Lightning
if keyboard_check_pressed(ord("V"))
{
lightning_form = 1
lightningmag1 = 1
global.form = 4
}
if lightning_form = 1
{
fire_form = 0
poison_form = 0
astral_form = 0
mirror_form = 0
movespeed = 4
if sattack = 1
    {
    instance_create(obj_player.x ,obj_player.y ,obj_lightdash_mag)
    }
}
else
{
movespeed = 3
}

//Mirror
if keyboard_check_pressed(ord("B"))
{
mirror_form = 1
mirrormag1 = 1
global.form = 5
}
if mirror_form = 1
{
fire_form = 0
poison_form = 0
lightning_form = 0
astral_form = 0
}

