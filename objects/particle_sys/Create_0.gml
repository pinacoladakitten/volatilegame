//Particle System
global.particles = part_system_create();
//Initialize Particles
scr_fire_init();
scr_slash_init();
scr_fire_ember_init();
scr_pcloud_init();
scr_enmexp_init();
scr_formswitch_init();
scr_dash_init();
scr_poison_drip_init();
scr_smshatter_init();

