/// @description Getting Inputs
key_right = keyboard_check(ord("D"));
key_left = -keyboard_check(ord("A"));
key_jump = keyboard_check_pressed(vk_space);

///Reaction to inputs
if (hurt = 0){
move = key_left + key_right;
hsp = move * movespeed;
}

//Gravity, Jumping, and Double Jumping
if (vsp < 10) vsp += grav;
if (dash = 1)
    {
if (place_meeting(x,y+1,obj_col))
{
        doublej = 1;
        vsp = key_jump * -jumpspeed
}
if (!place_meeting(x,y+1,obj_col))
{
if (doublej = 1)
if (keyboard_check_pressed(vk_space))
{
instance_create(obj_player.x,obj_player.y+15,obj_eff_doublej)
vsp = key_jump * -jumpspeed
doublej = 0;
}
    }
}

///Collision stuff

//Horizontal Collision
if (place_meeting(x+hsp,y,obj_col))
{
    while(!place_meeting(x+sign(hsp),y,obj_col))
    {
    x += sign(hsp);
    }
    hsp = 0;
}
x += hsp;
//Vertical Collision
if (place_meeting(x,y+vsp,obj_col))
{
    while(!place_meeting(x,y+sign(vsp),obj_col))
    {
    y += sign(vsp);
    }
    vsp = 0;
}
y += vsp;

if (hurt = 1)
{
move_bounce_solid(false)
}

///Sprites
image_speed=0.25
image_alpha = 0.5
image_blend = c_aqua
if (image_index = 3) 
if (attack = 1) 
attack = 0;

if attack = 0
if hurt = 0
{
    if (hsp > 0) image_xscale = 1
    if (hsp < 0) image_xscale = -1

    if (place_meeting(x,y+1,obj_col))
    {
        if (hsp = 0) 
        {
            sprite_index = spr_player_idle_m
        }
        if (hsp > 0 or hsp < 0)
        {
            sprite_index = spr_player_walk_m
        }
    }
    if (vsp < 1)
    if (!place_meeting(x,y+1,obj_col))
    {
        sprite_index = spr_player_jump_m
    }
    if (vsp > 1)
    if (!place_meeting(x,y+1,obj_col))
    {
        sprite_index = spr_player_fall_m
    }
    if dash = 0 {
    sprite_index = spr_player_dash_m
    }
}

