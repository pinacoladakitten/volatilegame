action_set_relative(1);
hit = 1;
if invincible = 0
{
audio_play_sound(snd_hurt02, 8, false);
instance_create(obj_snake.x,obj_snake.y,obj_eff_blood)
vsp = -3
snake_life = snake_life - random_range(2, 4);
invincible = 1
alarm[0] = room_speed * 0.1
}

var __b__;
__b__ = action_if_variable(invincible, 0, 0);
if __b__
{
with (obj_starball_mag) {
life += -1;
}
}
__b__ = action_if_variable(snake_life, 0, 3);
if __b__
{
part_particles_create(global.particles, x, y, global.pt_enmexp, 1);

}
action_set_relative(0);
