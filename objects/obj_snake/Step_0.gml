action_set_relative(1);
///Collision stuff

//Horizontal Collision
if (place_meeting(x+hsp,y,obj_col))
{
    while(!place_meeting(x+sign(hsp),y,obj_col))
    {
    x += sign(hsp);
    }
    hsp = 0;
}
//Vertical Collision
if (place_meeting(x,y+vsp,obj_col))
{
    while(!place_meeting(x,y+sign(vsp),obj_col))
    {
    y += sign(vsp);
    }
    vsp = 0;
}


x += hsp;
y += vsp;

image_speed = 0.2
if invincible = 0
{
if (hspeed > 0) image_xscale=1
if (hspeed < 0) image_xscale=-1
}
if snake_life < 0 {
audio_play_sound(snd_bat_dead, 10, false);
instance_destroy();
}
if (vsp < 5) vsp += grav;

//Movement
if image_index = 3
if (place_meeting(x,y+1,obj_col))
{
move_towards_point(obj_player.x,y,1.5)
}
if !(place_meeting(x,y+1,obj_col))
{
move_towards_point(obj_player.x,y,0)
}
move_bounce_solid(false)

var __b__;
__b__ = action_if_variable(snake_life, 0, 3);
if __b__
{
__b__ = action_if_dice(3);
if __b__
{
action_create_object(obj_item_mana, 0, 0);
}
}
__b__ = action_if_variable(snake_life, 0, 3);
if __b__
{
__b__ = action_if_dice(3);
if __b__
{
action_create_object(obj_item_health, 0, 0);
}
}
action_set_relative(0);
