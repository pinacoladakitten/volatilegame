/// @description Main event
draw_sprite(spr_textbox,0,__view_get( e__VW.XView, 0 )+10,__view_get( e__VW.YView, 0 )+500)

//The Text
if text = 1
{
str1 = @"Testing...testing...ehhhhh 
...umm, is this thing on?"
cstr = string_copy(str1,1,pos)
draw_text_ext_transformed(__view_get( e__VW.XView, 0 )+50,__view_get( e__VW.YView, 0 )+525,string_hash_to_newline(cstr),-1,-1,2,2,0)
pos+= 0.25
}

if text = 2
{
str2 = @"Yes? YES!? YOU CAN HEAR ME!?
AWWWW SNAP! WAY PAST COOL!"
cstr = string_copy(str2,1,pos)
draw_text_ext_transformed(__view_get( e__VW.XView, 0 )+50,__view_get( e__VW.YView, 0 )+525,string_hash_to_newline(cstr),-1,-1,2,2,0)
pos+= 0.25
}

if text = 3
{
str3 = "Oh, sorry...caps lock. :("
cstr = string_copy(str3,1,pos)
draw_text_ext_transformed(__view_get( e__VW.XView, 0 )+50,__view_get( e__VW.YView, 0 )+525,string_hash_to_newline(cstr),-1,-1,2,2,0)
pos+= 0.25
}

if text = 4
{
str4 = @"Anyway, I'm contacting you because...well
...I'm very lonely."
cstr = string_copy(str4,1,pos)
draw_text_ext_transformed(__view_get( e__VW.XView, 0 )+50,__view_get( e__VW.YView, 0 )+525,string_hash_to_newline(cstr),-1,-1,2,2,0)
pos+= 0.25
}

if text = 5
{
str5 = @"I need someone to annoy-...erm...talk to 
while I sit here."
cstr = string_copy(str5,1,pos)
draw_text_ext_transformed(__view_get( e__VW.XView, 0 )+50,__view_get( e__VW.YView, 0 )+525,string_hash_to_newline(cstr),-1,-1,2,2,0)
pos+= 0.25
}

if text = 6
{
str6 = "Listening to the Sonic Adventure vocal tracks."
cstr = string_copy(str6,1,pos)
draw_text_ext_transformed(__view_get( e__VW.XView, 0 )+50,__view_get( e__VW.YView, 0 )+525,string_hash_to_newline(cstr),-1,-1,2,2,0)
pos+= 0.25
}

if text = 7
{
str7 = "At 4 am."
cstr = string_copy(str7,1,pos)
draw_text_ext_transformed(__view_get( e__VW.XView, 0 )+50,__view_get( e__VW.YView, 0 )+525,string_hash_to_newline(cstr),-1,-1,2,2,0)
pos+= 0.25
}

if text = 8
{
str8 = "Brb, gotta go make memes."
cstr = string_copy(str8,1,pos)
draw_text_ext_transformed(__view_get( e__VW.XView, 0 )+50,__view_get( e__VW.YView, 0 )+525,string_hash_to_newline(cstr),-1,-1,2,2,0)
pos+= 0.25
}

draw_set_font(text_01)
draw_set_color(c_white)

