hit = 1;
if invincible = 0
{
audio_play_sound(snd_enm_hit, 8, false);
instance_create(x,y,obj_eff_blood)
instance_create(x,y,obj_eff_swhit01)
hspeed = hspeed * -5
vsp = -3
hsp = -2*sign(obj_player.x - x)
zombie_life = zombie_life - (global.meleedamage*0.5)
invincible = 1
alarm[0] = room_speed * 0.5
}

poisoned = 0.06
image_blend = c_green
alarm[2] = room_speed * 3;
part_particles_create(global.particles, x, y, global.pt_pdrip, 1);

var __b__;
__b__ = action_if_variable(zombie_life, 0, 3);
if __b__
{
part_particles_create(global.particles, x, y, global.pt_enmexp, 1);

}
