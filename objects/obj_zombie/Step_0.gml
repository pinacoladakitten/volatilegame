action_set_relative(1);
///Collision stuff

//Horizontal Collision
if (place_meeting(x+hsp,y,obj_col))
{
    while(!place_meeting(x+sign(hsp),y,obj_col))
    {
    x += sign(hsp);
    }
    hsp = 0;
}
//Vertical Collision
if (place_meeting(x,y+vsp,obj_col))
{
    while(!place_meeting(x,y+sign(vsp),obj_col))
    {
    y += sign(vsp);
    }
    vsp = 0;
}


x += hsp;
y += vsp;

image_speed = 0.2
if zombie_life < 1 {
part_particles_create(global.particles, x, y, global.pt_enmexp, 1);
audio_play_sound(snd_zombie_dead, 10, false);
instance_destroy();
}
if (vsp < 5) vsp += grav;

//Movement
if (place_meeting(x,y+1,obj_col))
{
hsp = image_xscale
}
if (place_meeting(x+hsp,y,obj_col))
{
image_xscale=image_xscale*-1
}
move_bounce_solid(false)

///Statuses
if (poisoned > 0)
{
zombie_life = zombie_life - poisoned
}

var __b__;
__b__ = action_if_variable(zombie_life, 0, 3);
if __b__
{
part_particles_create(global.particles, x, y, global.pt_enmexp, 1);

}
__b__ = action_if_variable(zombie_life, 0, 3);
if __b__
{
__b__ = action_if_dice(3);
if __b__
{
action_create_object(obj_item_mana, 0, 0);
}
}
__b__ = action_if_variable(zombie_life, 0, 3);
if __b__
{
__b__ = action_if_dice(3);
if __b__
{
action_create_object(obj_item_health, 0, 0);
}
}
action_set_relative(0);
