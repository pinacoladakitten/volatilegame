{
  "spriteId": {
    "name": "rocks",
    "path": "sprites/rocks/rocks.yy",
  },
  "tileWidth": 32,
  "tileHeight": 32,
  "tilexoff": 0,
  "tileyoff": 0,
  "tilehsep": 0,
  "tilevsep": 0,
  "spriteNoExport": false,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "out_tilehborder": 2,
  "out_tilevborder": 2,
  "out_columns": 1,
  "tile_count": 1,
  "autoTileSets": [],
  "tileAnimationFrames": [],
  "tileAnimationSpeed": 15.0,
  "tileAnimation": {
    "FrameData": [
      0,
    ],
    "SerialiseFrameCount": 1,
  },
  "macroPageTiles": {
    "SerialiseWidth": 0,
    "SerialiseHeight": 0,
    "TileSerialiseData": [],
  },
  "parent": {
    "name": "w1 tiles",
    "path": "folders/Tile Sets/w1 tiles.yy",
  },
  "resourceVersion": "1.0",
  "name": "rocks_tileset",
  "tags": [],
  "resourceType": "GMTileSet",
}